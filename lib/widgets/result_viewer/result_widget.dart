import 'package:flutter/material.dart';
import 'package:text_check/models/phone_screen.dart';
import 'package:text_check/models/text_properties.dart';
import 'package:text_check/widgets/result_viewer/text_widget.dart';

class ResultWidget extends StatelessWidget {
  final List<PhoneScreen> phoneScreens;
  final TextProperties properties;
  final String text;

  const ResultWidget({
    super.key,
    this.phoneScreens = PhoneScreen.all,
    required this.properties,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: phoneScreens.length + 2,
        itemBuilder: (context, index) {
          if (index == 0 || index == phoneScreens.length + 1) {
            return const SizedBox(height: 32);
          }
          return TextWidget(
            phoneScreen: phoneScreens[index - 1],
            properties: properties,
            text: text,
          );
        });
  }
}
