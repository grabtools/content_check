import 'package:flutter/material.dart';
import 'package:text_check/models/phone_screen.dart';
import 'package:text_check/models/text_alignment.dart';
import 'package:text_check/models/text_dimension_properties.dart';
import 'package:text_check/models/text_font.dart';
import 'package:text_check/models/text_properties.dart';

class TextWidget extends StatelessWidget {
  final PhoneScreen phoneScreen;
  final TextProperties properties;
  final String text;

  const TextWidget({
    super.key,
    required this.phoneScreen,
    required this.properties,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          phoneScreen.name,
          textDirection: TextDirection.ltr,
          style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
        ),
        const SizedBox(
          height: 16,
        ),
        _content,
        const SizedBox(
          height: 32,
        )
      ],
    );
  }

  Widget get _content {
    final isFixedWidth = properties.dimensionProperties.type == TextDimensionType.fixedWidth;
    return SizedBox(
      width: phoneScreen.width,
      child: Row(
        textDirection: TextDirection.ltr,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _makeBoundLine(),
          if (isFixedWidth) const Spacer(),
          if (isFixedWidth) _textBox else Expanded(child: _textBox),
          if (isFixedWidth) const Spacer(),
          _makeBoundLine(),
        ],
      ),
    );
  }

  Widget _makeBoundLine() => Container(
        height: 50,
        decoration: const BoxDecoration(color: Colors.black),
        width: 1,
      );

  Widget get _textBox {
    const decoration = BoxDecoration(
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: Color.fromARGB(33, 66, 66, 66),
          blurRadius: 5,
          blurStyle: BlurStyle.outer,
        )
      ],
    );

    return Container(
      decoration: decoration,
      width: properties.dimensionProperties.fixedWidth,
      padding: const EdgeInsets.only(top: 8, bottom: 8),
      margin: properties.dimensionProperties.spacing?.toInsets(),
      child: _text,
    );
  }

  Widget get _text {
    final isInvalidDimension =
        (properties.dimensionProperties.fixedWidth ?? double.infinity) > phoneScreen.width;
    if (properties.dimensionProperties.type == TextDimensionType.fixedWidth && isInvalidDimension) {
      return _errorText("Fixed width of text is larger than the screen's width");
    }

    if (properties.dimensionProperties.type == TextDimensionType.basedOnSpacing &&
        properties.dimensionProperties.spacing == null) {
      return _errorText("Spacing is misisng");
    }

    return Text(
      text,
      style: properties.font.toTextStyle(),
      textAlign: properties.alignment.toAlignment(),
      maxLines: properties.numberOfLines ?? 10000,
    );
  }

  Widget _errorText(String message) {
    return Text(
      message,
      style: const TextStyle(
        color: Colors.white,
        backgroundColor: Colors.red,
      ),
    );
  }
}

extension ToAlignment on TextAlignment {
  TextAlign toAlignment() {
    switch (this) {
      case TextAlignment.left:
        return TextAlign.left;
      case TextAlignment.center:
        return TextAlign.center;
      case TextAlignment.right:
        return TextAlign.right;
    }
  }
}

extension ToWeight on TextFontWeight {
  FontWeight toWeight() {
    switch (this) {
      case TextFontWeight.thin:
        return FontWeight.w100;
      case TextFontWeight.extraLight:
        return FontWeight.w200;
      case TextFontWeight.light:
        return FontWeight.w300;
      case TextFontWeight.normal:
        return FontWeight.w400;
      case TextFontWeight.medium:
        return FontWeight.w500;
      case TextFontWeight.semiBold:
        return FontWeight.w600;
      case TextFontWeight.bold:
        return FontWeight.w700;
      case TextFontWeight.extraBold:
        return FontWeight.w800;
      case TextFontWeight.black:
        return FontWeight.w900;
    }
  }
}

extension ToTextStyle on TextFont {
  TextStyle toTextStyle() => TextStyle(
        fontSize: size,
        fontWeight: weight.toWeight(),
        overflow: TextOverflow.ellipsis,
      );
}

extension ToInsets on TextSpacing {
  EdgeInsets toInsets() => EdgeInsets.only(left: leading, right: trailing);
}
