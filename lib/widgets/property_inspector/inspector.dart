import 'dart:async';

import 'package:flutter/material.dart';
import 'package:text_check/models/text_alignment.dart';
import 'package:text_check/models/text_dimension_properties.dart';
import 'package:text_check/models/text_font.dart';
import 'package:text_check/models/text_properties.dart';
import 'package:text_check/widgets/property_inspector/bottom_buttons.dart';
import 'package:text_check/widgets/property_inspector/dimension_inspector.dart';
import 'package:text_check/widgets/property_inspector/font_inspector.dart';
import 'package:text_check/widgets/property_inspector/misc_inspector.dart';
import 'package:text_check/widgets/property_inspector/text_inspector.dart';

class Inspector extends StatelessWidget {
  final String initialText;
  final TextProperties initialProperties;

  final void Function(String) onTextChanged;
  final void Function(double) onFontSizeChanged;
  final void Function(TextFontWeight) onFontWeightChanged;
  final void Function(int?) onNumberOfLinesChanged;
  final void Function(TextAlignment) onTextAlignmentChanged;
  final void Function(TextDimensionProperties) onDimensionChanged;
  final void Function() onImportPressed;
  final void Function() onExportPressed;

  final Stream<String> textStream;
  final Stream<TextProperties> propertiesStream;

  const Inspector({
    super.key,
    required this.initialText,
    required this.initialProperties,
    required this.onTextChanged,
    required this.onFontSizeChanged,
    required this.onFontWeightChanged,
    required this.onNumberOfLinesChanged,
    required this.onTextAlignmentChanged,
    required this.onDimensionChanged,
    required this.onImportPressed,
    required this.onExportPressed,
    required this.textStream,
    required this.propertiesStream,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          TextInspector(
            initialText: initialText,
            textStream: textStream,
            onChanged: onTextChanged,
          ),
          const SizedBox(height: 32),
          FontInspector(
            initialFont: initialProperties.font,
            fontStream: propertiesStream.map((properties) => properties.font),
            onSizeChanged: onFontSizeChanged,
            onWeightChanged: onFontWeightChanged,
          ),
          const SizedBox(height: 16),
          MiscInspector(
            initialNumberOfLines: initialProperties.numberOfLines,
            initialAlignment: initialProperties.alignment,
            propertiesStream: propertiesStream,
            onNumberOfLinesChanged: onNumberOfLinesChanged,
            onTextAlignmentChanged: onTextAlignmentChanged,
          ),
          const SizedBox(height: 16),
          DimensionInspector(
            initialProperties: initialProperties.dimensionProperties,
            propertiesStream: propertiesStream.map((properties) => properties.dimensionProperties),
            onChanged: onDimensionChanged,
          ),
          const SizedBox(height: 64),
          BottomButtons(
            onImportPressed: onImportPressed,
            onExportPressed: onExportPressed,
          ),
        ],
      ),
    );
  }
}
