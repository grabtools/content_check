import 'dart:async';

import 'package:flutter/material.dart';
import 'package:text_check/models/text_font.dart';

class FontInspector extends StatefulWidget {
  final TextFont initialFont;
  final Stream<TextFont> fontStream;
  final void Function(double) onSizeChanged;
  final void Function(TextFontWeight) onWeightChanged;

  const FontInspector({
    super.key,
    required this.initialFont,
    required this.fontStream,
    required this.onSizeChanged,
    required this.onWeightChanged,
  });

  @override
  State<FontInspector> createState() => _FontInspectorState();
}

class _FontInspectorState extends State<FontInspector> {
  late final TextEditingController _sizeController;
  TextFontWeight _weight = TextFontWeight.normal;
  late final StreamSubscription _subscription;

  @override
  void initState() {
    _sizeController = TextEditingController(text: "${widget.initialFont.size}");
    _weight = widget.initialFont.weight;
    _subscription = widget.fontStream.listen((font) {
      _sizeController.text = "${font.size}";
      _weight = font.weight;
    });
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(flex: 1, child: _fontTextField),
        const SizedBox(width: 8),
        Flexible(flex: 0, child: _weightSelector),
      ],
    );
  }

  Widget get _fontTextField => TextField(
        controller: _sizeController,
        decoration: const InputDecoration(
          labelText: "Font size",
          isDense: true,
          border: OutlineInputBorder(borderSide: BorderSide()),
        ),
        onChanged: (value) {
          final size = double.tryParse(value) ?? 14;
          widget.onSizeChanged(size);
        },
      );

  Widget get _weightSelector => DropdownButton<TextFontWeight>(
      value: _weight,
      items: TextFontWeight.values
          .map((e) => DropdownMenuItem<TextFontWeight>(value: e, child: Text(e.name)))
          .toList(),
      onChanged: (weight) {
        widget.onWeightChanged(weight ?? TextFontWeight.normal);
        setState(() {
          _weight = weight ?? TextFontWeight.normal;
        });
      });
}

extension GetName on TextFontWeight {
  String get name {
    switch (this) {
      case TextFontWeight.thin:
        return "Thin (100)";
      case TextFontWeight.extraLight:
        return "Extra-light (200)";
      case TextFontWeight.light:
        return "Light (300)";
      case TextFontWeight.normal:
        return "Normal (400)";
      case TextFontWeight.medium:
        return "Medium (500)";
      case TextFontWeight.semiBold:
        return "Semi-bold (600)";
      case TextFontWeight.bold:
        return "Bold (700)";
      case TextFontWeight.extraBold:
        return "Extra-bold (800)";
      case TextFontWeight.black:
        return "Black (900)";
    }
  }
}
