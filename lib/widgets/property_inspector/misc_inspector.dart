import 'dart:async';

import 'package:flutter/material.dart';
import 'package:text_check/models/text_alignment.dart';
import 'package:text_check/models/text_properties.dart';

class MiscInspector extends StatefulWidget {
  final int? initialNumberOfLines;
  final TextAlignment initialAlignment;
  final Stream<TextProperties> propertiesStream;
  final void Function(int?) onNumberOfLinesChanged;
  final void Function(TextAlignment) onTextAlignmentChanged;

  const MiscInspector({
    super.key,
    required this.initialNumberOfLines,
    required this.initialAlignment,
    required this.propertiesStream,
    required this.onNumberOfLinesChanged,
    required this.onTextAlignmentChanged,
  });

  @override
  State<MiscInspector> createState() => _MiscInspectorState();
}

class _MiscInspectorState extends State<MiscInspector> {
  late final TextEditingController _numberOfLinesController;
  TextAlignment _alignment = TextAlignment.left;
  late final StreamSubscription _subscription;

  @override
  void initState() {
    final numberOfLines = widget.initialNumberOfLines != null
        ? "${widget.initialNumberOfLines}"
        : "to infinity and beyond";
    _numberOfLinesController = TextEditingController(text: numberOfLines);
    _alignment = widget.initialAlignment;
    _subscription = widget.propertiesStream.listen((properties) {
      _numberOfLinesController.text = "${properties.numberOfLines}";
      _alignment = properties.alignment;
    });
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(child: _numberOfLinesTextField),
        const SizedBox(width: 8),
        _alignmentSelector,
      ],
    );
  }

  Widget get _numberOfLinesTextField => TextField(
        controller: _numberOfLinesController,
        decoration: const InputDecoration(
          labelText: "Number of lines",
          hintText: "blank/invalid/0 = no limit",
          isDense: true,
          border: OutlineInputBorder(borderSide: BorderSide()),
        ),
        onChanged: (numberOfLines) {
          final number = int.tryParse(numberOfLines);
          widget.onNumberOfLinesChanged(number == 0 ? null : number);
        },
      );

  Widget get _alignmentSelector => Row(
        children: [
          IconButton(
            color: _alignment == TextAlignment.left ? Colors.blue : Colors.black,
            onPressed: () {
              setState(() {
                _alignment = TextAlignment.left;
              });
              widget.onTextAlignmentChanged(TextAlignment.left);
            },
            icon: const Icon(Icons.align_horizontal_left),
          ),
          IconButton(
            color: _alignment == TextAlignment.center ? Colors.blue : Colors.black,
            onPressed: () {
              setState(() {
                _alignment = TextAlignment.center;
              });
              widget.onTextAlignmentChanged(TextAlignment.center);
            },
            icon: const Icon(Icons.align_horizontal_center),
          ),
          IconButton(
            color: _alignment == TextAlignment.right ? Colors.blue : Colors.black,
            onPressed: () {
              setState(() {
                _alignment = TextAlignment.right;
              });
              widget.onTextAlignmentChanged(TextAlignment.right);
            },
            icon: const Icon(Icons.align_horizontal_right),
          ),
        ],
      );
}
