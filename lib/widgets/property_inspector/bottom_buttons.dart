import 'package:flutter/material.dart';

class BottomButtons extends StatelessWidget {
  final void Function() onImportPressed;
  final void Function() onExportPressed;
  const BottomButtons({super.key, required this.onImportPressed, required this.onExportPressed});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
          flex: 1,
          child: _importButton,
        ),
        Flexible(
          flex: 1,
          child: _exportButton,
        ),
      ],
    );
  }

  Widget get _importButton => TextButton(
        onPressed: onImportPressed,
        child: const Text("Import"),
      );

  Widget get _exportButton => TextButton(
        onPressed: onExportPressed,
        child: const Text("Export"),
      );
}
