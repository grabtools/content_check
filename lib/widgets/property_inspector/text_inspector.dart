import 'dart:async';

import 'package:flutter/material.dart';

class TextInspector extends StatefulWidget {
  final String initialText;
  final Stream<String> textStream;
  final void Function(String) onChanged;

  const TextInspector({
    super.key,
    required this.initialText,
    required this.textStream,
    required this.onChanged,
  });

  @override
  State<TextInspector> createState() => _TextInspectorState();
}

class _TextInspectorState extends State<TextInspector> {
  late final TextEditingController _controller;
  late final StreamSubscription _subscription;

  @override
  void initState() {
    _controller = TextEditingController(text: widget.initialText);
    _subscription = widget.textStream.listen((text) {
      _controller.text = text;
    });
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _controller,
      decoration: const InputDecoration(
        labelText: "Text to test",
        border: OutlineInputBorder(borderSide: BorderSide()),
      ),
      keyboardType: TextInputType.multiline,
      minLines: 3,
      maxLines: 6,
      onChanged: widget.onChanged,
    );
  }
}
