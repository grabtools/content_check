import 'dart:async';

import 'package:flutter/material.dart';
import 'package:text_check/models/text_dimension_properties.dart';

class DimensionInspector extends StatefulWidget {
  final TextDimensionProperties initialProperties;
  final Stream<TextDimensionProperties> propertiesStream;
  final void Function(TextDimensionProperties) onChanged;

  const DimensionInspector({
    super.key,
    required this.initialProperties,
    required this.propertiesStream,
    required this.onChanged,
  });

  @override
  State<DimensionInspector> createState() => _DimensionInspectorState();
}

class _DimensionInspectorState extends State<DimensionInspector> {
  late final TextEditingController _leftSpacingController;
  late final TextEditingController _rightSpacingController;
  late final TextEditingController _widthController;
  TextDimensionType _type = TextDimensionType.basedOnSpacing;
  late final StreamSubscription _subscription;

  @override
  void initState() {
    final leading = widget.initialProperties.spacing?.leading;
    final trailing = widget.initialProperties.spacing?.trailing;
    final width = widget.initialProperties.fixedWidth;
    _leftSpacingController = TextEditingController(text: leading != null ? "$leading" : "");
    _rightSpacingController = TextEditingController(text: trailing != null ? "$trailing" : "");
    _widthController = TextEditingController(text: width != null ? "$width" : "");
    _type = widget.initialProperties.type;
    _subscription = widget.propertiesStream.listen((properties) {
      _leftSpacingController.text =
          properties.spacing?.leading != null ? "${properties.spacing?.leading}" : "";
      _rightSpacingController.text =
          properties.spacing?.trailing != null ? "${properties.spacing?.trailing}" : "";
      _widthController.text = properties.fixedWidth != null ? "${properties.fixedWidth}" : "";
      _type = properties.type;
    });
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _typeSelector,
        _sizeInput,
      ],
    );
  }

  Widget get _sizeInput => Container(
        decoration: BoxDecoration(border: Border.all()),
        child: Row(
          children: [
            Flexible(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: _leftSpacingController,
                  enabled: _type == TextDimensionType.basedOnSpacing,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    label: Center(
                      child: Text(_type == TextDimensionType.basedOnSpacing ? "Spacing" : "x"),
                    ),
                    isDense: true,
                    floatingLabelAlignment: FloatingLabelAlignment.center,
                  ),
                  onChanged: (value) {
                    final leadingSpacing = double.tryParse(value) ?? 0;
                    final trailingSpacing = double.tryParse(_rightSpacingController.text) ?? 0;
                    widget.onChanged(
                      TextDimensionProperties(
                        type: _type,
                        spacing: TextSpacing(leading: leadingSpacing, trailing: trailingSpacing),
                      ),
                    );
                  },
                ),
              ),
            ),
            Flexible(
              flex: 2,
              child: Container(
                decoration: BoxDecoration(
                  color: const Color.fromARGB(45, 0, 0, 0),
                  border: Border.all(),
                ),
                margin: const EdgeInsets.all(8.0),
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: _widthController,
                  enabled: _type == TextDimensionType.fixedWidth,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    label: Center(
                      child: Text(_type == TextDimensionType.fixedWidth ? "Width" : "x"),
                    ),
                    isDense: true,
                    floatingLabelAlignment: FloatingLabelAlignment.center,
                  ),
                  onChanged: (value) {
                    final width = double.tryParse(value) ?? 100;
                    widget.onChanged(
                      TextDimensionProperties(
                        type: _type,
                        fixedWidth: width == 0 ? 100 : width,
                      ),
                    );
                  },
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: _rightSpacingController,
                  enabled: _type == TextDimensionType.basedOnSpacing,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    label: Center(
                      child: Text(_type == TextDimensionType.basedOnSpacing ? "Spacing" : "x"),
                    ),
                    isDense: true,
                    floatingLabelAlignment: FloatingLabelAlignment.center,
                  ),
                  onChanged: (value) {
                    final leadingSpacing = double.tryParse(_leftSpacingController.text) ?? 0;
                    final trailingSpacing = double.tryParse(value) ?? 0;
                    widget.onChanged(
                      TextDimensionProperties(
                        type: _type,
                        spacing: TextSpacing(leading: leadingSpacing, trailing: trailingSpacing),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      );

  Widget get _typeSelector => Row(
        children: [
          const Text("Dimension is determined by  "),
          DropdownButton<TextDimensionType>(
              value: _type,
              items: TextDimensionType.values
                  .map((e) => DropdownMenuItem<TextDimensionType>(value: e, child: Text(e.name)))
                  .toList(),
              onChanged: (value) {
                final type = value ?? TextDimensionType.basedOnSpacing;
                switch (type) {
                  case TextDimensionType.fixedWidth:
                    final width = double.tryParse(_widthController.text) ?? 100;
                    widget.onChanged(
                      TextDimensionProperties(type: type, fixedWidth: width),
                    );
                    _leftSpacingController.text = "";
                    _rightSpacingController.text = "";
                    _widthController.text =
                        _widthController.text.isEmpty ? "100" : _widthController.text;
                    break;
                  case TextDimensionType.basedOnSpacing:
                    final leadingSpacing = double.tryParse(_leftSpacingController.text) ?? 0;
                    final trailingSpacing = double.tryParse(_rightSpacingController.text) ?? 0;
                    widget.onChanged(
                      TextDimensionProperties(
                        type: type,
                        spacing: TextSpacing(
                          leading: leadingSpacing,
                          trailing: trailingSpacing,
                        ),
                      ),
                    );
                    _leftSpacingController.text =
                        _leftSpacingController.text.isEmpty ? "0" : _leftSpacingController.text;
                    _rightSpacingController.text =
                        _rightSpacingController.text.isEmpty ? "0" : _rightSpacingController.text;
                    _widthController.text = "";
                    break;
                }
                setState(() {
                  _type = type;
                });
              }),
        ],
      );
}

extension GetName on TextDimensionType {
  String get name {
    switch (this) {
      case TextDimensionType.fixedWidth:
        return "Fixed Width";
      case TextDimensionType.basedOnSpacing:
        return "Spacing constraints";
    }
  }
}
