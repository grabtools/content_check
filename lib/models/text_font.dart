enum TextFontWeight { thin, extraLight, light, normal, medium, semiBold, bold, extraBold, black }

extension TextFontWeightCodable on TextFontWeight {
  static TextFontWeight fromJson(String s) {
    switch (s) {
      case "thin":
        return TextFontWeight.thin;
      case "extraLight":
        return TextFontWeight.extraLight;
      case "light":
        return TextFontWeight.light;
      case "normal":
        return TextFontWeight.normal;
      case "medium":
        return TextFontWeight.medium;
      case "semiBold":
        return TextFontWeight.semiBold;
      case "bold":
        return TextFontWeight.bold;
      case "extraBold":
        return TextFontWeight.extraBold;
      case "black":
        return TextFontWeight.black;
      default:
        return TextFontWeight.normal;
    }
  }

  String toJson() {
    switch (this) {
      case TextFontWeight.thin:
        return "thin";
      case TextFontWeight.extraLight:
        return "extraLight";
      case TextFontWeight.light:
        return "light";
      case TextFontWeight.normal:
        return "normal";
      case TextFontWeight.medium:
        return "medium";
      case TextFontWeight.semiBold:
        return "semiBold";
      case TextFontWeight.bold:
        return "bold";
      case TextFontWeight.extraBold:
        return "extraBold";
      case TextFontWeight.black:
        return "black";
    }
  }
}

class TextFont {
  final double size;
  final TextFontWeight weight;

  const TextFont({this.size = 14, this.weight = TextFontWeight.normal});

  TextFont.fromJson(Map<String, dynamic> map)
      : size = map["size"],
        weight = TextFontWeightCodable.fromJson(map["weight"]);

  TextFont copyWith({double? size, TextFontWeight? weight}) {
    return TextFont(
      size: size ?? this.size,
      weight: weight ?? this.weight,
    );
  }

  Map<String, dynamic> toJson() => {
        "size": size,
        "weight": weight.toJson(),
      };
}
