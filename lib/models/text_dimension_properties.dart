class TextSpacing {
  final double leading;
  final double trailing;

  const TextSpacing({this.leading = 0, this.trailing = 0});

  TextSpacing.fromJson(Map<String, dynamic> map)
      : leading = map["leading"],
        trailing = map["trailing"];

  static const zero = TextSpacing();

  TextSpacing copyWith({double? leading, double? trailing}) {
    return TextSpacing(
      leading: leading ?? this.leading,
      trailing: trailing ?? this.trailing,
    );
  }

  Map<String, dynamic> toJson() => {
        "leading": leading,
        "trailing": trailing,
      };
}

enum TextDimensionType { fixedWidth, basedOnSpacing }

extension TextDimensionTypeCodable on TextDimensionType {
  static TextDimensionType fromJson(String s) {
    switch (s) {
      case "fixedWidth":
        return TextDimensionType.fixedWidth;
      case "basedOnSpacing":
        return TextDimensionType.basedOnSpacing;
      default:
        return TextDimensionType.basedOnSpacing;
    }
  }

  String toJson() {
    switch (this) {
      case TextDimensionType.fixedWidth:
        return "fixedWidth";
      case TextDimensionType.basedOnSpacing:
        return "basedOnSpacing";
    }
  }
}

class TextDimensionProperties {
  static const standard = TextDimensionProperties(
    type: TextDimensionType.basedOnSpacing,
    fixedWidth: null,
    spacing: TextSpacing(),
  );

  final TextDimensionType type;
  final double? fixedWidth;
  final TextSpacing? spacing;

  const TextDimensionProperties({required this.type, this.fixedWidth, this.spacing});

  TextDimensionProperties.fromJson(Map<String, dynamic> map)
      : type = TextDimensionTypeCodable.fromJson(map["type"]),
        fixedWidth = map["fixedWidth"],
        spacing = TextSpacing.fromJson(map["spacing"]);

  TextDimensionProperties copyWith({
    TextDimensionType? type,
    double? fixedWidth,
    TextSpacing? spacing,
  }) {
    return TextDimensionProperties(
      type: type ?? this.type,
      fixedWidth: fixedWidth ?? this.fixedWidth,
      spacing: spacing ?? this.spacing,
    );
  }

  Map<String, dynamic> toJson() => {
        "type": type.toJson(),
        "fixedWidth": fixedWidth,
        "spacing": spacing?.toJson(),
      };
}
