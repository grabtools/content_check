import 'package:text_check/models/text_alignment.dart';
import 'package:text_check/models/text_font.dart';
import 'package:text_check/models/text_dimension_properties.dart';

class TextProperties {
  static const standard = TextProperties(
    numberOfLines: null,
    font: TextFont(),
    dimensionProperties: TextDimensionProperties.standard,
    alignment: TextAlignment.left,
  );

  final int? numberOfLines;
  final TextFont font;
  final TextDimensionProperties dimensionProperties;
  final TextAlignment alignment;

  const TextProperties({
    this.numberOfLines = 10000,
    required this.font,
    required this.dimensionProperties,
    required this.alignment,
  });

  TextProperties.fromJson(Map<String, dynamic> map)
      : numberOfLines = map["numberOfLines"],
        font = TextFont.fromJson(map["font"]),
        dimensionProperties = TextDimensionProperties.fromJson(map["dimensionProperties"]),
        alignment = TextAlignmentCodable.fromJson(map["alignment"]);

  TextProperties copyWith({
    int? numberOfLines,
    TextFont? font,
    TextDimensionProperties? dimensionProperties,
    TextAlignment? alignment,
  }) {
    return TextProperties(
        numberOfLines: numberOfLines ?? this.numberOfLines,
        font: font ?? this.font,
        dimensionProperties: dimensionProperties ?? this.dimensionProperties,
        alignment: alignment ?? this.alignment);
  }

  Map<String, dynamic> toJson() => {
        "numberOfLines": numberOfLines,
        "font": font.toJson(),
        "dimensionProperties": dimensionProperties.toJson(),
        "alignment": alignment.toJson(),
      };
}
