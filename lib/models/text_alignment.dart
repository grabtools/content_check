enum TextAlignment { left, center, right }

extension TextAlignmentCodable on TextAlignment {
  static TextAlignment fromJson(String s) {
    switch (s) {
      case "left":
        return TextAlignment.left;
      case "right":
        return TextAlignment.right;
      case "center":
        return TextAlignment.center;
      default:
        return TextAlignment.left;
    }
  }

  String toJson() {
    switch (this) {
      case TextAlignment.left:
        return "left";
      case TextAlignment.center:
        return "center";
      case TextAlignment.right:
        return "right";
    }
  }
}
