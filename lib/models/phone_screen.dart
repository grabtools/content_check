class PhoneScreen {
  static const iphoneSe1sGen = PhoneScreen(
    width: 320,
    name: "iPhone SE 1st Gen",
  );

  static const iphoneSe = PhoneScreen(
    width: 375,
    name: "iPhone SE",
  );

  static const iphone13 = PhoneScreen(
    width: 390,
    name: "iPhone 13",
  );

  static const iphone14Pro = PhoneScreen(
    width: 393,
    name: "iPhone 14 Pro",
  );

  static const iphone8Plus = PhoneScreen(
    width: 414,
    name: "iPhone 8 Plus",
  );

  static const iphone13ProMax = PhoneScreen(
    width: 428,
    name: "iPhone 13 Pro Max",
  );

  static const iphone14ProMax = PhoneScreen(
    width: 430,
    name: "iPhone 14 Pro Max",
  );

  static const samsumS10 = PhoneScreen(
    width: 360,
    name: "Samsung S10",
  );

  static const googlePixel3 = PhoneScreen(
    width: 393,
    name: "Google Pixel 3",
  );

  static const googlePixel3XL = PhoneScreen(
    width: 412,
    name: "Google Pixel 3 XL",
  );

  static const sonyXperiaZUltra = PhoneScreen(
    width: 540,
    name: "Sony Xperia Z Ultra",
  );

  static const samsungZFold2 = PhoneScreen(
    width: 884,
    name: "Samsung Z Fold2",
  );

  static const all = [
    PhoneScreen.iphoneSe1sGen,
    PhoneScreen.iphoneSe,
    PhoneScreen.iphone13,
    PhoneScreen.iphone14Pro,
    PhoneScreen.iphone8Plus,
    PhoneScreen.iphone13ProMax,
    PhoneScreen.iphone14ProMax,
    PhoneScreen.samsumS10,
    PhoneScreen.googlePixel3,
    PhoneScreen.googlePixel3XL,
    PhoneScreen.sonyXperiaZUltra,
    PhoneScreen.samsungZFold2,
  ];

  final double width;
  final String name;

  const PhoneScreen({required this.width, required String name}) : name = "$name ($width)";
}
