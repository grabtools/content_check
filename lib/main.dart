import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:text_check/models/text_properties.dart';
import 'package:text_check/widgets/property_inspector/inspector.dart';
import 'package:text_check/widgets/result_viewer/result_widget.dart';

void main() {
  runApp(MaterialApp(theme: ThemeData(fontFamily: 'SanomatSans'), home: const MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.

  String _text = "Hello World";
  TextProperties _properties = TextProperties.standard;
  late final TextEditingController _configController;

  final StreamController<String> _textStreamController = StreamController.broadcast();
  final StreamController<TextProperties> _propertiesStreamController = StreamController.broadcast();

  @override
  void initState() {
    _configController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: MediaQuery(
        data: const MediaQueryData(),
        child: Directionality(
          textDirection: TextDirection.ltr,
          child: Row(children: [_result, _inspector]),
        ),
      ),
    );
  }

  Widget get _result => Expanded(
        child: ResultWidget(
          properties: _properties,
          text: _text,
        ),
      );

  Widget get _inspector {
    return Container(
      decoration: const BoxDecoration(color: Colors.black12),
      width: 400,
      child: Column(
        children: [
          Inspector(
            initialText: _text,
            initialProperties: _properties,
            onTextChanged: (text) {
              setState(() {
                _text = text;
              });
            },
            onFontSizeChanged: (size) {
              setState(() {
                _properties = _properties.copyWith(font: _properties.font.copyWith(size: size));
              });
            },
            onFontWeightChanged: (weight) {
              setState(() {
                _properties = _properties.copyWith(font: _properties.font.copyWith(weight: weight));
              });
            },
            onNumberOfLinesChanged: (numberOfLines) {
              setState(() {
                _properties = _properties.copyWith(numberOfLines: numberOfLines);
              });
            },
            onTextAlignmentChanged: (alignment) {
              setState(() {
                _properties = _properties.copyWith(alignment: alignment);
              });
            },
            onDimensionChanged: (dimension) {
              setState(() {
                _properties = _properties.copyWith(dimensionProperties: dimension);
              });
            },
            onImportPressed: () {
              showDialog<String>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text("Input your config below"),
                  content: SizedBox(
                    width: 500,
                    child: TextField(
                      controller: _configController,
                      decoration: const InputDecoration(
                        labelText: "Text to test",
                        border: OutlineInputBorder(borderSide: BorderSide()),
                      ),
                      keyboardType: TextInputType.multiline,
                      minLines: 6,
                      maxLines: 6,
                    ),
                  ),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                        setState(() {
                          _properties = TextProperties.fromJson(jsonDecode(_configController.text));
                        });
                        _propertiesStreamController.add(_properties);
                      },
                      child: const Text('OK'),
                    ),
                  ],
                ),
              );
            },
            onExportPressed: () {
              final json = jsonEncode(_properties.toJson());
              showDialog<String>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text("You can copy config below to share"),
                  content: Container(
                    decoration: BoxDecoration(border: Border.all()),
                    padding: const EdgeInsets.all(16),
                    child: SelectableText(json),
                  ),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text('OK'),
                    ),
                  ],
                ),
              );
            },
            textStream: _textStreamController.stream,
            propertiesStream: _propertiesStreamController.stream,
          ),
        ],
      ),
    );
  }
}
